@foreach($tweets as $key => $value)
    <tr>
        {!! Form::hidden('tweet_id', $value['id'], ['id' => 'tweet_id']) !!}
        <td>{{ $value['full_text'] }}</td>
        <td>
            @if(!empty($value['extended_entities']['media']))
                @foreach($value['extended_entities']['media'] as $item)
                    <a href="{{ $item['media_url_https'] }}" class="fancybox">
                        <img id="image" src="{{ $item['media_url_https'] }}">
                    </a>
                @endforeach
            @else
                <img id="image" src="{{asset('/images/no-image.png')}}">
            @endif
        </td>
        <td>{{ $value['favorite_count'] }}</td>
        <td>{{ $value['retweet_count'] }}</td>
    </tr>
@endforeach