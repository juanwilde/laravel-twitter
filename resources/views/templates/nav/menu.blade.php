<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}">Laravel</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            {!! Form::open(['route' => 'search', 'method' => 'POST', 'class' => 'navbar-form navbar-left']) !!}
            <div class="form-group">
                {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Nombre de usuario']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Buscar', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</nav>