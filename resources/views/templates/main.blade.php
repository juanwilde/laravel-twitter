<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | Laravel Twitter</title>
    <link rel="stylesheet" href="{{ asset('/assets/bootstrap/dist/css/bootstrap.css') }}" type="text/css">
    @yield('css')
</head>
<body>

<div id="main-wrapper" class="col-md-10 col-md-offset-1">
    @include('templates.nav.menu')
    <div id="errors">
        @include('vendor.flash.message')
        @if(count($errors) > 0)
            <div class="alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    @yield('content')
</div>
<script type="text/javascript" src="{{ asset('/assets/jquery/dist/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('/assets/bootstrap/dist/js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/document-ready.js') }}"></script>
@yield('js')
</body>
</html>