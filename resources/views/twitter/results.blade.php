@extends('templates.main')

@section('title')
    Resultados para {{ $username }}
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('/assets/fancybox/source/jquery.fancybox.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}" type="text/css">
@endsection

@section('content')
    @if(count($tweets) > 0)
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Mensaje</th>
                <th>Imágenes</th>
                <th>Favoritos</th>
                <th>Retweets</th>
            </tr>
            </thead>
            <tbody id="tweets">
            @foreach($tweets as $key => $value)
                <tr>
                    {!! Form::hidden('tweet_id', $value['id'], ['id' => 'tweet_id']) !!}
                    <td>{{ $value['full_text'] }}</td>
                    <td>
                        @if(!empty($value['extended_entities']['media']))
                            @foreach($value['extended_entities']['media'] as $item)
                                <a href="{{ $item['media_url_https'] }}" class="fancybox">
                                    <img id="image" src="{{ $item['media_url_https'] }}">
                                </a>
                            @endforeach
                        @else
                            <img id="image" src="{{asset('/images/no-image.png')}}">
                        @endif
                    </td>
                    <td>{{ $value['favorite_count'] }}</td>
                    <td>{{ $value['retweet_count'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! Form::hidden('username', $username, ['id' => 'username']) !!}
        <div class="col-md-6 col-md-offset-3">
            <button id="load-more-tweets-btn" class="btn btn-info btn-block">Cargar más</button>
        </div>
        @else
        <div class="alert-danger">{{ NO_RESULTS }}</div>
    @endif
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('/assets/fancybox/source/jquery.fancybox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/load-fancybox.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/load-more-tweets.js') }}"></script>
@endsection