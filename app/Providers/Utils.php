<?php

namespace App\Providers;

use Twitter;

class Utils
{
    public static function getTweets($username, $lastTweetId = false)
    {
        $options = [
            'screen_name' => $username,
            'count'       => DEFAULT_NUMBER_OF_TWEETS,
            'format'      => 'array',
            'tweet_mode'  => 'extended'
        ];

        if ($lastTweetId) {
            $options['max_id'] = $lastTweetId;
        }

        try {
            $tweets = Twitter::getUserTimeLine($options);

            return $tweets;
        } catch (\Exception $e) {

            return false;
        }
    }
}