<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomRequest;
use App\Providers\Utils;
use Flash;

class TwitterController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function search(CustomRequest $request)
    {
        $tweets = Utils::getTweets($request->username);

        if (count($tweets) > 0) {
            return view('twitter.results')->with(['username' => $request->username, 'tweets' => $tweets]);
        }

        Flash::warning(NO_RESULTS);

        return redirect()->route('home');
    }
}
