<?php

namespace App\Http\Controllers;

use App\Providers\Utils;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function loadMoreTweets(Request $request)
    {
        if ($request->ajax()) {
            $tweets = Utils::getTweets($request->username, $request->last_tweet_id);

            if (count($tweets) > 0) {
                return new JsonResponse(view('templates.partials.tweets')
                    ->with(['username' => $request->username, 'tweets' => $tweets])->render(), 200);
            }

            return new JsonResponse(array(
                'message' => NO_RESULTS
            ), 200);
        }

        return new JsonResponse(array(
            'message' => NO_XML_RESQUEST
        ), 400);
    }
}
