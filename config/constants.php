<?php

if(!defined('DEFAULT_NUMBER_OF_TWEETS')) define('DEFAULT_NUMBER_OF_TWEETS', 6);

if(!defined('NO_RESULTS')) define('NO_RESULTS', 'No hay resultados...');

if(!defined('API_ERROR')) define('API_ERROR', 'Se ha producido un error al consultar la API');

if(!defined('NO_XML_RESQUEST')) define('NO_XML_RESQUEST', 'No es una petición AJAX válida');

