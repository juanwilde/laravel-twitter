<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', [
    'uses' => 'TwitterController@index',
    'as'   => 'home'
]);

Route::post('search', [
    'uses' => 'TwitterController@search',
    'as'   => 'search'
]);

Route::post('load-more-tweets', [
    'uses' => 'AjaxController@loadMoreTweets',
    'as'   => 'load.more.tweets'
]);
