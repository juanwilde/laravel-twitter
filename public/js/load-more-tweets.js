$('#load-more-tweets-btn').on('click', function () {

    var username = $('#username').val();
    var last_tweet_id = $('input#tweet_id:last').val();

    $.ajax({
        type: 'POST',
        url: 'load-more-tweets',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            username: username,
            last_tweet_id: last_tweet_id
        },
        dataType: 'json',
        success: function (response) {
            $('#tweets').append(response);
            $('.fancybox').fancybox();
            $('html, body').animate({scrollTop: $(document).height()}, 1000);
        },
        error: function (error) {
            console.log(error);
        }
    });
});
